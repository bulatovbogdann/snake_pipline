# Pipeline

## Description of the steps
1. **read_data** retrieves iris flower data from the sklearn library.

2. Data preprocessing is divided into two stages:

    - **data_preprocess_head** - take the first 30 entries.
    
    - **data_preprocess_tail** - take the last 30 entries.

3. Define two models (from the sklearn library):

    - **RandomForest**
    
    - **LogReg**

4. Train our models on all possible preprocessed data. As a result, we obtain four models: 2 types of models x 2 types of preprocessed data:

    - **model_head_forest.pkl**
    - **model_tail_forest.pkl**
    - **model_head_logreg.pkl**
    - **model_tail_logreg.pkl**

![Pipline](https://i.ibb.co/NYfSmHf/workflow-dag.png)
