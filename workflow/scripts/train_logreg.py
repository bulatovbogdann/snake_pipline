import pandas as pd
from sklearn.linear_model import LogisticRegression
import joblib


df = pd.read_csv(snakemake.input[0])
y = df["target"]
X = df.drop("target", axis=1)

model = LogisticRegression(max_iter=1000)
model.fit(X, y)

# Сохранение обученной модели в файл
joblib.dump(model, snakemake.output[0])