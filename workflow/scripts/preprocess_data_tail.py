import pandas as pd

df = pd.read_csv("workflow/result/iris.csv").tail(30)
df.to_csv(snakemake.output[0], index=False)