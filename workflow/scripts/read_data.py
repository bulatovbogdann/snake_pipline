import pandas as pd
from sklearn.datasets import load_iris

iris = load_iris()
df = pd.DataFrame(data=iris.data, columns=iris.feature_names)
df["target"] = iris.target
df = df.sample(df.shape[0])

df.to_csv(snakemake.output[0], index=False)